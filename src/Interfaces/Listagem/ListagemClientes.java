package Interfaces.Listagem;

import DAO.DAOCliente;

import Interfaces.Detalhamento.DetalharClientes;
import Interfaces.Modificacao.ModificarClientes;
import Interfaces.Remocao.RemoverCliente;

public class ListagemClientes extends javax.swing.JFrame {

    public ListagemClientes() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista_Clientes = new javax.swing.JList<>();
        cmdListar_Cliente = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jB_detalhar_Cliente = new javax.swing.JButton();
        jB_remover_Cliente = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Lista de Clientes");

        lista_Clientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lista_ClientesMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lista_Clientes);

        cmdListar_Cliente.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        cmdListar_Cliente.setText("Listar");
        cmdListar_Cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdListar_ClienteActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Loja de Instrumentos Musicais");

        jB_detalhar_Cliente.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jB_detalhar_Cliente.setText("Detalhar");
        jB_detalhar_Cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_detalhar_ClienteActionPerformed(evt);
            }
        });

        jB_remover_Cliente.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jB_remover_Cliente.setText("Remover");
        jB_remover_Cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_remover_ClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(148, 148, 148)
                .addComponent(cmdListar_Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jB_detalhar_Cliente)
                .addGap(18, 18, 18)
                .addComponent(jB_remover_Cliente)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdListar_Cliente)
                    .addComponent(jB_detalhar_Cliente)
                    .addComponent(jB_remover_Cliente))
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdListar_ClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdListar_ClienteActionPerformed
        DAOCliente lC = new DAOCliente();
        
        lC.listarClientes(this.lista_Clientes);
    }//GEN-LAST:event_cmdListar_ClienteActionPerformed

    private void lista_ClientesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lista_ClientesMouseClicked
        ModificarClientes mC = new ModificarClientes();
        DAOCliente cC = new DAOCliente();
        
        mC.setLocationRelativeTo(null);
        mC.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        mC.setVisible(true);
        cC.consultarClientes(Integer.parseInt(this.lista_Clientes.getModel().getElementAt(this.lista_Clientes.getSelectedIndex())));
    }//GEN-LAST:event_lista_ClientesMouseClicked

    private void jB_detalhar_ClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_detalhar_ClienteActionPerformed
        DetalharClientes dC = new DetalharClientes();
        
        dC.setLocationRelativeTo(null);
        dC.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dC.setVisible(true);
    }//GEN-LAST:event_jB_detalhar_ClienteActionPerformed

    private void jB_remover_ClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_remover_ClienteActionPerformed
        RemoverCliente rC = new RemoverCliente();
        
        rC.setLocationRelativeTo(null);
        rC.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        rC.setVisible(true);
    }//GEN-LAST:event_jB_remover_ClienteActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ListagemClientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdListar_Cliente;
    private javax.swing.JButton jB_detalhar_Cliente;
    private javax.swing.JButton jB_remover_Cliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lista_Clientes;
    // End of variables declaration//GEN-END:variables
}
