package Interfaces.Listagem;

import DAO.DAOInstrumento;

import Interfaces.Insercao.InserirVenda;
import Interfaces.Detalhamento.DetalharInstrumentosMusicais;
import Interfaces.Modificacao.ModificarInstrumentosMusicais;
import Interfaces.Remocao.RemoverInstrumentoMusical;

public class ListagemInstrumentosMusicais extends javax.swing.JFrame {

    public ListagemInstrumentosMusicais() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        cmdListar_Instrumento = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        lista_Instrumentos = new javax.swing.JList<>();
        jB_detalhar_InstrumentosMusicais = new javax.swing.JButton();
        jB_vendas_InstrumentosMusicais = new javax.swing.JButton();
        jB_remover_InstrumentosMusicais = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel8.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Loja de Instrumentos Musicais");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Lista de Instrumentos");

        cmdListar_Instrumento.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        cmdListar_Instrumento.setText("Listar");
        cmdListar_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdListar_InstrumentoActionPerformed(evt);
            }
        });

        lista_Instrumentos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lista_InstrumentosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(lista_Instrumentos);

        jB_detalhar_InstrumentosMusicais.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jB_detalhar_InstrumentosMusicais.setText("Detalhar");
        jB_detalhar_InstrumentosMusicais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_detalhar_InstrumentosMusicaisActionPerformed(evt);
            }
        });

        jB_vendas_InstrumentosMusicais.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jB_vendas_InstrumentosMusicais.setText("Vendas");
        jB_vendas_InstrumentosMusicais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_vendas_InstrumentosMusicaisActionPerformed(evt);
            }
        });

        jB_remover_InstrumentosMusicais.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        jB_remover_InstrumentosMusicais.setText("Remover");
        jB_remover_InstrumentosMusicais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_remover_InstrumentosMusicaisActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(141, 141, 141)
                .addComponent(cmdListar_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jB_detalhar_InstrumentosMusicais)
                .addGap(18, 18, 18)
                .addComponent(jB_remover_InstrumentosMusicais)
                .addGap(18, 18, 18)
                .addComponent(jB_vendas_InstrumentosMusicais)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(150, Short.MAX_VALUE)
                .addComponent(jLabel8)
                .addGap(137, 137, 137))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addGap(22, 22, 22)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 128, Short.MAX_VALUE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdListar_Instrumento)
                    .addComponent(jB_detalhar_InstrumentosMusicais)
                    .addComponent(jB_vendas_InstrumentosMusicais)
                    .addComponent(jB_remover_InstrumentosMusicais))
                .addGap(23, 23, 23))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdListar_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdListar_InstrumentoActionPerformed
        DAOInstrumento lI = new DAOInstrumento();
        
        lI.listarInstrumentos(this.lista_Instrumentos);
    }//GEN-LAST:event_cmdListar_InstrumentoActionPerformed

    private void lista_InstrumentosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lista_InstrumentosMouseClicked
        ModificarInstrumentosMusicais mI = new ModificarInstrumentosMusicais();
        DAOInstrumento cI = new DAOInstrumento();
        
        mI.setLocationRelativeTo(null);
        mI.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        mI.setVisible(true);
     
        Object aux = lista_Instrumentos.getModel().getElementAt(this.lista_Instrumentos.getSelectedIndex()); 
        cI.consultarInstrumentos(Integer.parseInt(String.valueOf(aux)));
    }//GEN-LAST:event_lista_InstrumentosMouseClicked

    private void jB_detalhar_InstrumentosMusicaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_detalhar_InstrumentosMusicaisActionPerformed
        DetalharInstrumentosMusicais dI = new DetalharInstrumentosMusicais();
        
        dI.setLocationRelativeTo(null);
        dI.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dI.setVisible(true);
    }//GEN-LAST:event_jB_detalhar_InstrumentosMusicaisActionPerformed

    private void jB_vendas_InstrumentosMusicaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_vendas_InstrumentosMusicaisActionPerformed
        InserirVenda iV = new InserirVenda();
        
        iV.setLocationRelativeTo(null);
        iV.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        iV.setVisible(true);
    }//GEN-LAST:event_jB_vendas_InstrumentosMusicaisActionPerformed

    private void jB_remover_InstrumentosMusicaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_remover_InstrumentosMusicaisActionPerformed
        RemoverInstrumentoMusical rI = new RemoverInstrumentoMusical();
        
        rI.setLocationRelativeTo(null);
        rI.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        rI.setVisible(true);
    }//GEN-LAST:event_jB_remover_InstrumentosMusicaisActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ListagemInstrumentosMusicais().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdListar_Instrumento;
    private javax.swing.JButton jB_detalhar_InstrumentosMusicais;
    private javax.swing.JButton jB_remover_InstrumentosMusicais;
    private javax.swing.JButton jB_vendas_InstrumentosMusicais;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JList<String> lista_Instrumentos;
    // End of variables declaration//GEN-END:variables
}
