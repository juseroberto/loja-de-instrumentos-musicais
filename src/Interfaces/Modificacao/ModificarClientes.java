package Interfaces.Modificacao;

import DAO.DAOCliente;

public class ModificarClientes extends javax.swing.JFrame {

    public ModificarClientes() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNome_ModificarClientes = new javax.swing.JTextField();
        txtCpf_ModificarClientes = new javax.swing.JTextField();
        txtCodigo_ModificarClientes = new javax.swing.JTextField();
        txtEmail_ModificarClientes = new javax.swing.JTextField();
        txtData_Nascimento_ModificarClientes = new javax.swing.JTextField();
        txtTelefone_ModificarClientes = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        cmdModificar_Cliente = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Loja de Instrumentos Musicais");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Modificar Cliente");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("CPF:");

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Insira o código do cliente que terá as informações atualizadas:");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("E-mail:");

        jLabel6.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel6.setText("Data de Nascimento:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("Telefone:");

        txtNome_ModificarClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtNome_ModificarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNome_ModificarClientesActionPerformed(evt);
            }
        });

        txtCpf_ModificarClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCpf_ModificarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCpf_ModificarClientesActionPerformed(evt);
            }
        });

        txtCodigo_ModificarClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtCodigo_ModificarClientes.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtCodigo_ModificarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigo_ModificarClientesActionPerformed(evt);
            }
        });

        txtEmail_ModificarClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtEmail_ModificarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEmail_ModificarClientesActionPerformed(evt);
            }
        });

        txtData_Nascimento_ModificarClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtData_Nascimento_ModificarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtData_Nascimento_ModificarClientesActionPerformed(evt);
            }
        });

        txtTelefone_ModificarClientes.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtTelefone_ModificarClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefone_ModificarClientesActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText("Nome:");

        cmdModificar_Cliente.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        cmdModificar_Cliente.setText("Modificar");
        cmdModificar_Cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdModificar_ClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(txtNome_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(2, 2, 2)
                                        .addComponent(txtEmail_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel8))
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(jLabel6))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(txtTelefone_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtData_Nascimento_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(txtCpf_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 262, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(110, 110, 110)
                                .addComponent(jLabel4))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtCodigo_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmdModificar_Cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(19, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNome_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCpf_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel7)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtEmail_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelefone_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtData_Nascimento_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtCodigo_ModificarClientes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cmdModificar_Cliente)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNome_ModificarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNome_ModificarClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNome_ModificarClientesActionPerformed

    private void txtCpf_ModificarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCpf_ModificarClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCpf_ModificarClientesActionPerformed

    private void txtCodigo_ModificarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigo_ModificarClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigo_ModificarClientesActionPerformed

    private void txtEmail_ModificarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEmail_ModificarClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEmail_ModificarClientesActionPerformed

    private void txtData_Nascimento_ModificarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtData_Nascimento_ModificarClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtData_Nascimento_ModificarClientesActionPerformed

    private void txtTelefone_ModificarClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefone_ModificarClientesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelefone_ModificarClientesActionPerformed

    private void cmdModificar_ClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdModificar_ClienteActionPerformed
        DAOCliente mC = new DAOCliente();  
        
        mC.modificarClientes((Integer.parseInt(txtCodigo_ModificarClientes.getText())), txtCpf_ModificarClientes.getText(),txtNome_ModificarClientes.getText(), txtData_Nascimento_ModificarClientes.getText(), txtEmail_ModificarClientes.getText(), txtTelefone_ModificarClientes.getText());
    }//GEN-LAST:event_cmdModificar_ClienteActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificarClientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdModificar_Cliente;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField txtCodigo_ModificarClientes;
    private javax.swing.JTextField txtCpf_ModificarClientes;
    private javax.swing.JTextField txtData_Nascimento_ModificarClientes;
    private javax.swing.JTextField txtEmail_ModificarClientes;
    private javax.swing.JTextField txtNome_ModificarClientes;
    private javax.swing.JTextField txtTelefone_ModificarClientes;
    // End of variables declaration//GEN-END:variables

    
}
