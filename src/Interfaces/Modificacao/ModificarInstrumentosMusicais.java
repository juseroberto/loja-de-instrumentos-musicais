package Interfaces.Modificacao;

import DAO.DAOInstrumento;

import Modelos.InstrumentoMusical;

public class ModificarInstrumentosMusicais extends javax.swing.JFrame {

    public ModificarInstrumentosMusicais() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtEstoque_ModificarInstrumentos = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtNome_ModificarInstrumentos = new javax.swing.JTextField();
        txtClassificacao_ModificarInstrumentos = new javax.swing.JTextField();
        cmdModificar_Instrumento = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jCB_Codigo_ModificarInstrumentos = new javax.swing.JComboBox<>();
        jB_Listar_ModificarInstrumentos = new javax.swing.JButton();
        txtPreco_ModificarInstrumentos = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Loja de Instrumentos Musicais");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Modificar Instrumento");

        txtEstoque_ModificarInstrumentos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtEstoque_ModificarInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEstoque_ModificarInstrumentosActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel8.setText("Nome do Instrumento:");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Classificação do Instrumento:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("Em estoque:");

        txtNome_ModificarInstrumentos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtNome_ModificarInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNome_ModificarInstrumentosActionPerformed(evt);
            }
        });

        txtClassificacao_ModificarInstrumentos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtClassificacao_ModificarInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClassificacao_ModificarInstrumentosActionPerformed(evt);
            }
        });

        cmdModificar_Instrumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cmdModificar_Instrumento.setText("Modificar");
        cmdModificar_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdModificar_InstrumentoActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setText("Selecione o código do instrumento que sofrerá alterações: ");

        jCB_Codigo_ModificarInstrumentos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCB_Codigo_ModificarInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCB_Codigo_ModificarInstrumentosActionPerformed(evt);
            }
        });

        jB_Listar_ModificarInstrumentos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jB_Listar_ModificarInstrumentos.setText("Listar dados");
        jB_Listar_ModificarInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jB_Listar_ModificarInstrumentosActionPerformed(evt);
            }
        });

        txtPreco_ModificarInstrumentos.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtPreco_ModificarInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPreco_ModificarInstrumentosActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel10.setText("Preço:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 380, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel8)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txtClassificacao_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(txtNome_ModificarInstrumentos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel5)))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel7)
                                    .addComponent(txtPreco_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel10)
                                    .addComponent(txtEstoque_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(91, 91, 91)
                        .addComponent(jCB_Codigo_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(89, 89, 89)
                        .addComponent(cmdModificar_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jB_Listar_ModificarInstrumentos)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2)
                .addGap(18, 18, 18)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCB_Codigo_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNome_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEstoque_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtClassificacao_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPreco_ModificarInstrumentos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdModificar_Instrumento)
                    .addComponent(jB_Listar_ModificarInstrumentos))
                .addGap(20, 20, 20))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtEstoque_ModificarInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEstoque_ModificarInstrumentosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEstoque_ModificarInstrumentosActionPerformed

    private void txtNome_ModificarInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNome_ModificarInstrumentosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNome_ModificarInstrumentosActionPerformed

    private void txtClassificacao_ModificarInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClassificacao_ModificarInstrumentosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClassificacao_ModificarInstrumentosActionPerformed

    private void cmdModificar_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdModificar_InstrumentoActionPerformed
        DAOInstrumento mI = new DAOInstrumento();
        InstrumentoMusical iM = (InstrumentoMusical) jCB_Codigo_ModificarInstrumentos.getModel().getSelectedItem();
        
        mI.modificarInstrumentos(iM.getCodigo(), txtNome_ModificarInstrumentos.getText(), txtClassificacao_ModificarInstrumentos.getText(), (Double.parseDouble(txtPreco_ModificarInstrumentos.getText())), (Integer.parseInt(txtEstoque_ModificarInstrumentos.getText())));
    }//GEN-LAST:event_cmdModificar_InstrumentoActionPerformed

    private void jCB_Codigo_ModificarInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCB_Codigo_ModificarInstrumentosActionPerformed

    }//GEN-LAST:event_jCB_Codigo_ModificarInstrumentosActionPerformed

    private void jB_Listar_ModificarInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jB_Listar_ModificarInstrumentosActionPerformed
        DAOInstrumento sI = new DAOInstrumento();
        
        sI.selecaoInstrumentos(jCB_Codigo_ModificarInstrumentos);
    }//GEN-LAST:event_jB_Listar_ModificarInstrumentosActionPerformed

    private void txtPreco_ModificarInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPreco_ModificarInstrumentosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPreco_ModificarInstrumentosActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificarInstrumentosMusicais().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdModificar_Instrumento;
    private javax.swing.JButton jB_Listar_ModificarInstrumentos;
    private javax.swing.JComboBox<String> jCB_Codigo_ModificarInstrumentos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtClassificacao_ModificarInstrumentos;
    private javax.swing.JTextField txtEstoque_ModificarInstrumentos;
    private javax.swing.JTextField txtNome_ModificarInstrumentos;
    private javax.swing.JTextField txtPreco_ModificarInstrumentos;
    // End of variables declaration//GEN-END:variables

}
