package Interfaces;

import Interfaces.Insercao.InserirCliente;
import Interfaces.Insercao.InserirInstrumentoMusical;
import Interfaces.Listagem.ListagemClientes;
import Interfaces.Listagem.ListagemInstrumentosMusicais;

public class TelaInicial extends javax.swing.JFrame {

    public TelaInicial() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jMenuBar2 = new javax.swing.JMenuBar();
        jMenu_LIM = new javax.swing.JMenu();
        cadastroClientes = new javax.swing.JMenuItem();
        listagemClientes = new javax.swing.JMenuItem();
        cadastroInstrumentos = new javax.swing.JMenuItem();
        listagemInstrumentos = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/ImagesForBackground/loja_instrumentos_musicais.png"))); // NOI18N
        jLabel1.setLabelFor(this);

        jMenuBar2.setFont(new java.awt.Font("Aase Light", 1, 12)); // NOI18N

        jMenu_LIM.setText("Loja de Instrumentos Musicais");
        jMenu_LIM.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N

        cadastroClientes.setText("Cadastro de Clientes");
        cadastroClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroClientesActionPerformed(evt);
            }
        });
        jMenu_LIM.add(cadastroClientes);

        listagemClientes.setText("Listagem de Clientes");
        listagemClientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listagemClientesActionPerformed(evt);
            }
        });
        jMenu_LIM.add(listagemClientes);

        cadastroInstrumentos.setText("Cadastro de Instrumentos");
        cadastroInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cadastroInstrumentosActionPerformed(evt);
            }
        });
        jMenu_LIM.add(cadastroInstrumentos);

        listagemInstrumentos.setText("Listagem de Instrumentos");
        listagemInstrumentos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                listagemInstrumentosActionPerformed(evt);
            }
        });
        jMenu_LIM.add(listagemInstrumentos);

        jMenuBar2.add(jMenu_LIM);

        setJMenuBar(jMenuBar2);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void cadastroClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroClientesActionPerformed
        InserirCliente iC = new InserirCliente();

        iC.setLocationRelativeTo(null);
        iC.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        iC.setVisible(true);
    }//GEN-LAST:event_cadastroClientesActionPerformed

    private void listagemClientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listagemClientesActionPerformed
        ListagemClientes lC = new ListagemClientes();

        lC.setLocationRelativeTo(null);
        lC.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        lC.setVisible(true);
    }//GEN-LAST:event_listagemClientesActionPerformed

    private void cadastroInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cadastroInstrumentosActionPerformed
        InserirInstrumentoMusical iM = new InserirInstrumentoMusical();

        iM.setLocationRelativeTo(null);
        iM.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        iM.setVisible(true);
    }//GEN-LAST:event_cadastroInstrumentosActionPerformed

    private void listagemInstrumentosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_listagemInstrumentosActionPerformed
        ListagemInstrumentosMusicais lI = new ListagemInstrumentosMusicais();

        lI.setLocationRelativeTo(null);
        lI.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        lI.setVisible(true);
    }//GEN-LAST:event_listagemInstrumentosActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new TelaInicial().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenuItem cadastroClientes;
    private javax.swing.JMenuItem cadastroInstrumentos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar2;
    private javax.swing.JMenu jMenu_LIM;
    private javax.swing.JMenuItem listagemClientes;
    private javax.swing.JMenuItem listagemInstrumentos;
    // End of variables declaration//GEN-END:variables
}
