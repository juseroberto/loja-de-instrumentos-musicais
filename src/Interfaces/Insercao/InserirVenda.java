 package Interfaces.Insercao;

import DAO.DAOFuncionario;
import DAO.DAOInstrumento;
import DAO.DAOVenda;
 
import Modelos.Funcionario;

public class InserirVenda extends javax.swing.JFrame {

    public InserirVenda() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jCB_CPF = new javax.swing.JComboBox<>();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jCB_CodigoFuncionario_Venda = new javax.swing.JComboBox<>();
        cmdListarCB_Instrumento = new javax.swing.JButton();
        txtTotalArrecadado_Venda = new javax.swing.JTextField();
        jCB_CodigoInstrumento_Venda = new javax.swing.JComboBox<>();
        txtQuantidadeVendida_Venda = new javax.swing.JTextField();
        cmdRegistrarVenda_Instrumento = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();

        jCB_CPF.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCB_CPF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCB_CPFActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel8.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Loja de Instrumentos Musicais");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Registro de Vendas");

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Código do Funcionário:");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel3.setText("Código do instrumento:");

        jCB_CodigoFuncionario_Venda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCB_CodigoFuncionario_Venda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCB_CodigoFuncionario_VendaActionPerformed(evt);
            }
        });

        cmdListarCB_Instrumento.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        cmdListarCB_Instrumento.setText("Listar");
        cmdListarCB_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdListarCB_InstrumentoActionPerformed(evt);
            }
        });

        jCB_CodigoInstrumento_Venda.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jCB_CodigoInstrumento_Venda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCB_CodigoInstrumento_VendaActionPerformed(evt);
            }
        });

        cmdRegistrarVenda_Instrumento.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        cmdRegistrarVenda_Instrumento.setText("Registrar");
        cmdRegistrarVenda_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdRegistrarVenda_InstrumentoActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel4.setText("Quantidade vendida:");

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Total arrecadado:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jCB_CodigoFuncionario_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jCB_CodigoInstrumento_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(cmdListarCB_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel4)
                            .addComponent(txtQuantidadeVendida_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTotalArrecadado_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, 270, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmdRegistrarVenda_Instrumento)))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(46, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCB_CodigoFuncionario_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtQuantidadeVendida_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jCB_CodigoInstrumento_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTotalArrecadado_Venda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 23, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmdRegistrarVenda_Instrumento)
                    .addComponent(cmdListarCB_Instrumento))
                .addGap(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmdListarCB_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdListarCB_InstrumentoActionPerformed
        DAOFuncionario sF = new DAOFuncionario();
        DAOInstrumento sI = new DAOInstrumento();
        
        sI.selecaoInstrumentos(jCB_CodigoInstrumento_Venda);
        sF.selecaoFuncionario(jCB_CodigoFuncionario_Venda);
    }//GEN-LAST:event_cmdListarCB_InstrumentoActionPerformed

    private void jCB_CodigoFuncionario_VendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCB_CodigoFuncionario_VendaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCB_CodigoFuncionario_VendaActionPerformed

    private void jCB_CPFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCB_CPFActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCB_CPFActionPerformed

    private void jCB_CodigoInstrumento_VendaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCB_CodigoInstrumento_VendaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCB_CodigoInstrumento_VendaActionPerformed

    private void cmdRegistrarVenda_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdRegistrarVenda_InstrumentoActionPerformed
        DAOVenda iV = new DAOVenda();
        Funcionario f = (Funcionario) jCB_CodigoFuncionario_Venda.getModel().getSelectedItem();
        Integer iM = (Integer) jCB_CodigoInstrumento_Venda.getModel().getSelectedItem();
 
        iV.inserirVendas(f.getCodigo(), iM, (Integer.parseInt(txtQuantidadeVendida_Venda.getText())), (Double.parseDouble(txtTotalArrecadado_Venda.getText())));
    }//GEN-LAST:event_cmdRegistrarVenda_InstrumentoActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InserirVenda().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdListarCB_Instrumento;
    private javax.swing.JButton cmdRegistrarVenda_Instrumento;
    private javax.swing.JComboBox<String> jCB_CPF;
    private javax.swing.JComboBox<String> jCB_CodigoFuncionario_Venda;
    private javax.swing.JComboBox<String> jCB_CodigoInstrumento_Venda;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JTextField txtQuantidadeVendida_Venda;
    private javax.swing.JTextField txtTotalArrecadado_Venda;
    // End of variables declaration//GEN-END:variables
}
