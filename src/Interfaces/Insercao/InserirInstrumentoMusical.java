package Interfaces.Insercao;

import DAO.DAOInstrumento;

public class InserirInstrumentoMusical extends javax.swing.JFrame {

    public InserirInstrumentoMusical() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        txtNome_Instrumento = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cmdAdicionar_InstrumentoMusical = new javax.swing.JButton();
        txtClassificacao_Instrumento = new javax.swing.JTextField();
        txtPreco_Instrumento = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txtEstoque_Instrumento = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel8.setFont(new java.awt.Font("Arial", 1, 24)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Loja de Instrumentos Musicais");

        jLabel1.setFont(new java.awt.Font("Arial", 0, 20)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Cadastro de Instrumentos Musicais");

        txtNome_Instrumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtNome_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNome_InstrumentoActionPerformed(evt);
            }
        });

        jLabel5.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel5.setText("Classificação do instrumento:");

        jLabel7.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel7.setText("Quantidade em estoque:");

        cmdAdicionar_InstrumentoMusical.setFont(new java.awt.Font("Arial", 0, 13)); // NOI18N
        cmdAdicionar_InstrumentoMusical.setText("Adicionar");
        cmdAdicionar_InstrumentoMusical.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmdAdicionar_InstrumentoMusicalActionPerformed(evt);
            }
        });

        txtClassificacao_Instrumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtClassificacao_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtClassificacao_InstrumentoActionPerformed(evt);
            }
        });

        txtPreco_Instrumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtPreco_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPreco_InstrumentoActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel2.setText("Nome:");

        txtEstoque_Instrumento.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        txtEstoque_Instrumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEstoque_InstrumentoActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        jLabel9.setText("Preço:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(txtClassificacao_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel5))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtPreco_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel9)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel7))))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(txtNome_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(txtEstoque_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, 225, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(198, 198, 198)
                .addComponent(cmdAdicionar_InstrumentoMusical)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtNome_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtEstoque_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtClassificacao_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtPreco_Instrumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(cmdAdicionar_InstrumentoMusical)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtNome_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNome_InstrumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNome_InstrumentoActionPerformed

    private void cmdAdicionar_InstrumentoMusicalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmdAdicionar_InstrumentoMusicalActionPerformed
        DAOInstrumento iIM = new DAOInstrumento();

        iIM.inserirInstrumentos(txtNome_Instrumento.getText(), txtClassificacao_Instrumento.getText(), (Double.parseDouble(txtPreco_Instrumento.getText())), (Integer.parseInt(txtEstoque_Instrumento.getText())));
    }//GEN-LAST:event_cmdAdicionar_InstrumentoMusicalActionPerformed

    private void txtClassificacao_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtClassificacao_InstrumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtClassificacao_InstrumentoActionPerformed

    private void txtPreco_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPreco_InstrumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPreco_InstrumentoActionPerformed

    private void txtEstoque_InstrumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEstoque_InstrumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtEstoque_InstrumentoActionPerformed

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InserirInstrumentoMusical().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton cmdAdicionar_InstrumentoMusical;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txtClassificacao_Instrumento;
    private javax.swing.JTextField txtEstoque_Instrumento;
    private javax.swing.JTextField txtNome_Instrumento;
    private javax.swing.JTextField txtPreco_Instrumento;
    // End of variables declaration//GEN-END:variables
}
