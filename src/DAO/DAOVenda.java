
package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DAOVenda {
    
        public void inserirVendas(Integer codigo_funcionario, Integer codigo_instrumento, Integer quantidade_vendida, double total_arrecadado) {

        try {
            Connection c = DAOConexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_lojadeinstrumentosmusicais.venda(codigo_funcionario, codigo_instrumento, quantidade_vendida, total_arrecadado) values (?, ?, ?, ?)");

            ps.setInt(1, codigo_funcionario);
            ps.setInt(2, codigo_instrumento);
            ps.setInt(3, quantidade_vendida);
            ps.setDouble(4, total_arrecadado);
            
            ps.executeUpdate();
            
            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }


}
