package DAO;

import Modelos.InstrumentoMusical;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DAOInstrumento {

    public void inserirInstrumentos(String nome, String classificacao, double preco, Integer estoque) {

        try {
            Connection c = DAOConexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_lojadeinstrumentosmusicais.instrumento(nome, classificacao, preco, estoque) values (?, ?, ?, ?)");

            ps.setString(1, nome);
            ps.setString(2, classificacao);
            ps.setDouble(3, preco);
            ps.setInt(4, estoque);

            ps.executeUpdate();

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void modificarInstrumentos(Integer codigo, String nome, String classificacao, double preco, Integer estoque) {
        try {
            Connection c = DAOConexao.obterConexao();

            String SQL = "update sc_lojadeinstrumentosmusicais.instrumento set nome = ?, classificacao = ?, preco = ?, estoque = ? where codigo = ?";

            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setString(1, nome);
            ps.setString(2, classificacao);
            ps.setDouble(3, preco);
            ps.setInt(4, estoque);
            ps.setInt(5, codigo);

            ps.executeUpdate();

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void removerInstrumentos(Integer codigo) {

        try {
            Connection c = DAOConexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("delete from sc_lojadeinstrumentosmusicais.instrumento where codigo = ?");

            ps.setInt(1, codigo);

            ps.execute();

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listarInstrumentos(JList listaInstrumentos) {

        try {
            listaInstrumentos.removeAll();

            DefaultListModel dfm = new DefaultListModel();
            Connection c = DAOConexao.obterConexao();
            String SQL = "select codigo, nome, classificacao, preco, estoque from sc_lojadeinstrumentosmusicais.instrumento";

            PreparedStatement ps = c.prepareStatement(SQL);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                dfm.addElement(rs.getInt("codigo"));
            }

            listaInstrumentos.setModel(dfm);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void detalharInstrumentos(JTable tabelaInstrumentos) {
        try {
            DefaultTableModel dtm = new DefaultTableModel();
            dtm = (DefaultTableModel) tabelaInstrumentos.getModel();
            Connection c = DAOConexao.obterConexao();

            String SQL = "select codigo, nome, classificacao, preco, estoque from sc_lojadeinstrumentosmusicais.instrumento";

            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);

            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getInt("codigo"), rs.getString("nome"), rs.getString("classificacao"), rs.getDouble("preco"), rs.getInt("estoque")});
            }
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

      public void selecaoInstrumentos(JComboBox auxSelecaoInstrumento) {
        try {
            DefaultComboBoxModel dcbm = new DefaultComboBoxModel();

            Connection c = DAOConexao.obterConexao();

            PreparedStatement ps;

            ps = c.prepareStatement("select codigo from sc_lojadeinstrumentosmusicais.instrumento");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                InstrumentoMusical iM = new InstrumentoMusical(rs.getInt("codigo"), null, null, 0.0, 0);

                dcbm.addElement(iM.getCodigo());
            }

            auxSelecaoInstrumento.setModel(dcbm);

            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void consultarInstrumentos(Integer codigo) {
        try {
            Connection c = DAOConexao.obterConexao();

            String SQL = "select codigo, nome, classificacao, preco, estoque from sc_lojadeinstrumentosmusicais.instrumento where codigo = ?";
            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setInt(1, codigo);

            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                InstrumentoMusical iM = new InstrumentoMusical();
                
                iM.setCodigo(rs.getInt("codigo"));
                iM.setNome(rs.getString("nome"));
                iM.setClassificacao(rs.getString("classificacao"));
                iM.setPreco(rs.getDouble("preco"));
                iM.setEstoque(rs.getInt("estoque"));
            }
            
            c.close();

        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
