package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DAOConexao {

    public static Connection obterConexao() {

        try {

            //IF
            Class.forName("org.postgresql.Driver");

            String banco = "jdbc:postgresql://10.90.24.54/jose";
            String usuario = "jose_roberto";
            String senha = "596321awhn";
            return DriverManager.getConnection(banco, usuario, senha);

            //Home
            /*Class.forName("org.postgresql.Driver");

            String banco = "jdbc:postgresql://127.0.0.1/homeDB";
            String usuario = "jose";
            String senha = "63524170asdfgh";
            return DriverManager.getConnection(banco, usuario, senha);*/
            
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
