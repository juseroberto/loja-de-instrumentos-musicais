package DAO;

import Modelos.Funcionario;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;

public class DAOFuncionario {

    //Não sei qual motivo me levou a criar isto.
    /*public void listarFuncionarios(JList listaFuncionarios) {

        try {
            listaFuncionarios.removeAll();

            DefaultListModel dfm = new DefaultListModel();
            Connection c = DAOConexao.obterConexao();
            String SQL = "select cpf, rg, nome, data_nascimento, email, data_admissao from sc_lojadeinstrumentosmusicais.funcionario";

            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery(SQL);

            while (rs.next()) {
                dfm.addElement(rs.getString("cpf"));
            }

            listaFuncionarios.setModel(dfm);

            c.close();
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    public void selecaoFuncionario(JComboBox auxSelecaoFuncionario) {
        try {
            DefaultComboBoxModel dcbm = new DefaultComboBoxModel();

            Connection c = DAOConexao.obterConexao();

            PreparedStatement ps;

            ps = c.prepareStatement("select codigo from sc_lojadeinstrumentosmusicais.funcionario");

            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
                Funcionario f = new Funcionario(rs.getInt("codigo"));

                dcbm.addElement(f);
            }

            auxSelecaoFuncionario.setModel(dcbm);
        } catch (SQLException ex) {
            Logger.getLogger(DAOInstrumento.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
