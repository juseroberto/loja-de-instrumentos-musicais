package DAO;

import Modelos.Cliente;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

public class DAOCliente {

    public void inserirClientes(String cpf, String nome, String data_nascimento, String email, String telefone) {

        try {
            Connection c = DAOConexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("insert into sc_lojadeinstrumentosmusicais.cliente(cpf, nome, data_nascimento, email, telefone) values (?, ?, ?, ?, ?)");

            ps.setString(1, cpf);
            ps.setString(2, nome);
            ps.setString(3, data_nascimento);
            ps.setString(4, email);
            ps.setString(5, telefone);

            ps.executeUpdate();

            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void modificarClientes(Integer codigo, String cpf, String nome, String data_nascimento, String email, String telefone) {
        try {
            Connection c = DAOConexao.obterConexao();

            String SQL = "update sc_lojadeinstrumentosmusicais.cliente set cpf = ?, nome = ?, data_nascimento = ?, email = ?, telefone = ? where codigo = ?";

            PreparedStatement ps = c.prepareStatement(SQL);

            ps.setString(1, cpf);
            ps.setString(2, nome);
            ps.setString(3, data_nascimento);
            ps.setString(4, email);
            ps.setString(5, telefone);
            ps.setInt(6, codigo);

            ps.executeUpdate();

            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void removerClientes(Integer codigo) {

        try {
            Connection c = DAOConexao.obterConexao();
            PreparedStatement ps = c.prepareStatement("delete from sc_lojadeinstrumentosmusicais.cliente where codigo = ?");

            ps.setInt(1, codigo);

            ps.executeUpdate();

            c.close();
           
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listarClientes(JList listaClientes) {

        try {
            listaClientes.removeAll();

            DefaultListModel dfm = new DefaultListModel();
            Connection c = DAOConexao.obterConexao();
            String SQL = "select codigo, cpf, nome, data_nascimento, email, telefone from sc_lojadeinstrumentosmusicais.cliente";

            Statement ps = c.createStatement();
            ResultSet rs = ps.executeQuery(SQL);

            while (rs.next()) {
                dfm.addElement(rs.getString("codigo"));
            }

            listaClientes.setModel(dfm);

            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void detalharClientes(JTable tabelaClientes) {
        try {
            DefaultTableModel dtm = new DefaultTableModel();
            dtm = (DefaultTableModel) tabelaClientes.getModel();
            Connection c = DAOConexao.obterConexao();

            String SQL = "select codigo, cpf, nome, data_nascimento, email, telefone from sc_lojadeinstrumentosmusicais.cliente";

            Statement s = c.createStatement();
            ResultSet rs = s.executeQuery(SQL);

            while (rs.next()) {
                dtm.addRow(new Object[]{rs.getInt("codigo"), rs.getString("cpf"), rs.getString("nome"), rs.getString("data_nascimento"), rs.getString("email"), rs.getString("telefone")});
            }
            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void consultarClientes(Integer codigo) {
        try {
            Connection c = DAOConexao.obterConexao();

            String SQL = "select codigo, cpf, nome, data_nascimento, email, telefone from sc_lojadeinstrumentosmusicais.cliente where codigo = ?";
            PreparedStatement ps = c.prepareStatement(SQL);
            
            ps.setInt(1, codigo);
            
            ResultSet rs = ps.executeQuery();

            if (rs.next()) {
                Cliente cliente = new Cliente();
                
                cliente.setCodigo(rs.getInt("codigo"));
                cliente.setCpf(rs.getString("cpf"));
                cliente.setNome(rs.getString("nome"));
                cliente.setData_nascimento(rs.getString("data_nascimento"));
                cliente.setEmail(rs.getString("email"));
                cliente.setTelefone(rs.getString("telefone"));
            }

            c.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(DAOCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
