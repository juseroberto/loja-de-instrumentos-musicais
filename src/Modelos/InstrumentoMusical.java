package Modelos;

public class InstrumentoMusical {

    private int codigo;
    private String nome;
    private String classificacao;
    private double preco;
    private int estoque;

    public InstrumentoMusical() {
    }

    public InstrumentoMusical(int codigo, String nome, String classificacao, double preco, int estoque) {
        this.codigo = codigo;
        this.nome = nome;
        this.classificacao = classificacao;
        this.preco = preco;
        this.estoque = estoque;
    }

    public int getCodigo() {
        return this.codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getClassificacao() {
        return this.classificacao;
    }

    public void setClassificacao(String classificacao) {
        this.classificacao = classificacao;
    }

    public double getPreco() {
        return this.preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public int getEstoque() {
        return this.estoque;
    }

    public void setEstoque(int estoque) {
        this.estoque = estoque;
    }

    @Override
    public String toString() {
        return this.codigo + this.nome + this.classificacao + this.preco + this.estoque;
    }

}
