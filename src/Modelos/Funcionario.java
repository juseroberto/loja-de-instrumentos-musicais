package Modelos;

public class Funcionario {

    private int codigo;

    public Funcionario() {
    }

    public Funcionario(int codigo) {
        this.codigo = codigo;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String toString() {
        return "" + codigo;
    }

}
